# shell scripts
Some shell scripts that I use.
The most of simple scripts are written on POSIX shell.

## Scripts overview
Standalone (one-file) scripts stores in [scripts directory](scripts), see their [README](scripts/README.md) for more info.
Large scripts with extra data files stores in separate directories with additional README file, see table below for short overview.

| Script             | Short description         |
| ------------------ | ------------------------- |
| [bdi](bdi)         | Beck Depression Inventory |
| [trtt](trtt)       | CLI train timetable       |

## Getting started
### Prerequisites
Some scripts require some dependencies, see the specific README file.

### Installing
Just run installation script
```shell
bash ./install
```
and follow his instructions.
Use `Space` to toggle items and `j`/`k` or arrows to move around, `Enter` for confirm choice.

> Why installation script is a bloated bash script?

Just cause it easy to make cool selection menu and it's not should be run often.

## License
These scripts are licensed under the terms of the GPLv3 license, see [here](LICENSE) for more info.
