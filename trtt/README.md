# trtt
**tr**ain **t**ime **t**able or **tr**ain **t**u **t**u.
Parse tutu.ru timetable and print it in terminal. No any browser needed.

## Why?
Simple minimalistic tool for viewing train time table without running any java scripts.
Also requires only around 0.06 Mb web traffic instead at least 0.1 MB traffic,
depends on browser cache and unnecessary requests blocking policy (at worst it would be 1+ Mb).

## Getting started
### Requirements
- [fzf](https://github.com/junegunn/fzf): a command-line fuzzy finder

### Create data for railway direction
For working script you need station info file
(default path `~/.local/share/trtt/stations.dat`, see script variable `station_file`) that looks like
```
<number> <station name>
```
You can generate this file with the script automatically.
`tutu.ru` has pages with all station of some railway direction, you need the link of a info page
and pass link to `trtt` script, for example
```console
trtt -g https://www.tutu.ru/01.php
```
It downloads given URL and generates file from it (path `~/.local/share/trtt/stations.dat`, see script variable `station_file`).
If file with this name is already exist script will do nothing. Rename or delete existing file or edit script variable `station_file`.
Validate file and continue, also after changing stations file highly recommends to clean trtt cache.

### Configuration
The first lines of the script have block that you should to edit.
```bash
# config
default_st1=111
default_st2=222
base_URL="https://www.tutu.ru/rasp.php"
station_file="${XDG_DATA_HOME:-$HOME/.local/share}/trtt/stations.dat"
```
Default means that these stations would be chosen without `-s` argument.
- `default_st1`: default departure station code.
- `default_st2`: default arrival station code.
- `base_URL`: first part of URL for request, see explanation below.
For example, for Kazan `tutu.ru` uses sub domain name `https://kazan.tutu.ru`,
so you would set `base_URL` for correct work with your stations.
- `station_file`: path to file with stations codes and names (see previous section)

## Usage
```
trtt [-hsb] [-d dd[.mm[.yyyy]]] [-g URL]
```
Run with `-h` for more info.

Consider that dates are too far from today’s (about a few months on average)
can be cause of error while extracting data.
If something going wrong, try to clean cache files in `${XDG_CACHE_HOME:-$HOME/.cache}/trtt`.
Automatic cache clean is not yet implemented...

## Roadmap
- [ ] Cache autoclean.
- [ ] Color indication for Lastochka train.
- [ ] Date validate: "29/30/31 Feb", "31 Apr" issues.
- [ ] Option for printing trains after some time: any filter of output.
- [ ] Technical information about loading by option, print if take info from cache: just verbose mode?
- [ ] Choose station file (railway direction) with base URL etc, more advanced config block or even config file.
- [ ] Parallel request with param `-b`.
