#!/bin/sh

usage()
{ 
cat << EOF
videostab: stabilize input videos with ffmpeg vidstab library.
There's the simple vaapi support, see script for details.

Usage: videostab [-hc] file1 [file2 ...]
Output videos titles would be "<input>-stab.mp4"/"<input>-comp.mp4".
Options:
  -h        Show help
  -c        Also make compare video from horizontaly stacked input and output videos ("<input>-comp.mp4").
EOF
}

check_ffmpeg_vidstab()
{
    ffmpeg -hide_banner -version | grep --quiet -e '--enable-libvidstab' || \
        { echo "libvidstab is not enabled in ffmpeg. Try to update ffmpeg version or compile it manually."; exit 1; }
}

stabilize_video()
{
    inpt_video="$1"
    extn_video="${inpt_video##*.}"
    stab_video="${inpt_video%.$extn_video}-stab.$extn_video"
    comp_video="${inpt_video%.$extn_video}-comp.$extn_video"

    ffprog="ffmpeg -hide_banner -hwaccel vaapi -hwaccel_device /dev/dri/renderD128"
    $ffprog -i "$inpt_video" -vf vidstabdetect -f null - &&
    $ffprog -i "$inpt_video" -vf vidstabtransform "$stab_video" &&
    [ "$comp" ] && $ffprog -i "$inpt_video" -i "$stab_video" -filter_complex hstack "$comp_video"
    rm -f transforms.trf
}

while getopts "hc" o; do case "${o}" in
     h) usage; exit 0 ;;
     c) comp=1; shift ;;
     ?) echo "Invalid option." >&2; usage; exit 1 ;;
esac done

check_ffmpeg_vidstab
for video_file in "$@"; do
    [ -f "$video_file" ] && stabilize_video "$video_file" || printf "File \033[2m$video_file\033[0m doesn't exist. Skipped.\n"
done
