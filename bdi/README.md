# BDI
This is a just the bash implementation of the Beck Depression Inventory.
The Beck Depression Inventory (BDI) is a 21-item,
self-report rating inventory that measures characteristic attitudes and symptoms of depression.
This implementation is BDI (II).
See more [here](https://www.apa.org/pi/about/publications/caregivers/practice-settings/assessment/tools/beck-depression)

## Why?
Beck Depression Inventory is one of the most widely used psychometric tests for measuring the severity of depression. 
If you try to find an online BDI implementation you will probably end up on the site with some privacy issues
such as the presence of some kind system for tracking site users of their actions.
Mental health is very private information and it not need to be shared with unknown third parties on the Internet.
So we should running BDI locally.
Also many sites has an extremely inconvenient GUI that requires a lot of unnecessary actions (extra mouse clicks),
which are easily solved using keyboard input and CLI that is one of the best patterns of communication with a computer.

Script has option for plotting saved inventory results using `gnuplot`,
see example plot of random BDI results below.
![plot](ex-plot.webp)

> Why bash not POSIX compatible shell?

A bit of bloatware to improve coding experience.
Also I wrote that script when I don't know anything about POSIX and now I don't think that it's worth to rewrite.

## Requirements
- [gnuplot](http://www.gnuplot.info/) (optional, for `-p` options, see below)

## Usage
```console
bdi [-hlp]
```
For more info run with `bdi -h`. Run `bdi` to run inventory.
Just follow script instructions and choose answers with digits keys and `y`/`n` keys.
Script can save result to log file `bdi_log_file` (see [configuration](#simple-configuration)) in the following format:
```
%Y-%m-%d-T%H:%M NNNNNNNNNNNNNNNNNNNNN C S T
```
- `%Y-%m-%d-T%H:%M`: timestamp
- `NNNNNNNNNNNNNNNNNNNNN`: individual symptoms scores (0-3)
- `c`: cognitive-affective score
- `S`: somatic score
- `T`: total (severity) score

For quickly viewing this file with default `$PAGER` (or less) just run `bdi -l`.

Run `bdi -p` to plot saved log file results with `gnuplot`
and open pdf plot via `xdg-open` (no replot if log file is newer than plot file).

### Simple configuration
The first lines of the bash script have variables `bdi_log_file` and `bdi_log_plot`
that define files paths for inventory result log and plot files.
Change it if you want. Do not forget to create directories if necessary.
```bash
bdi_log_file="${XDG_DATA_HOME:-$HOME/.local/share}/bdi/bdi.log"
bdi_log_plot="${XDG_DATA_HOME:-$HOME/.local/share}/bdi/plot.pdf"
```

## Roadmap
- [x] Plot function for local results (using gnuplot)
- [ ] ~~Set default options for "printing individual symptoms?" and "save result?".~~
- [x] `bdi -l` as shortcut for `cat bdi_log_file`.
- [ ] Issue: blinking while redrawing text in some terminals. It seems to be terminal problem.
